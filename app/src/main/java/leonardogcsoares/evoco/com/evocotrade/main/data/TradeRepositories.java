package leonardogcsoares.evoco.com.evocotrade.main.data;

import android.support.annotation.NonNull;

/**
 * Created by leonardogcsoares on 11/29/2015.
 */
public class TradeRepositories {

    private TradeRepositories() {}

    private static TradeRepository repository = null;

    public synchronized static TradeRepository getInMemoryRepoInstance(@NonNull UserParseServiceApi userParseServiceApi, @NonNull TradeParseServiceApi tradeParseServiceApi) {
        if (null == repository) {
            repository = new InMemoryTradesRepository(userParseServiceApi, tradeParseServiceApi);
        }

        return repository;
    }

}

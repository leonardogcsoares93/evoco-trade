package leonardogcsoares.evoco.com.evocotrade.main.login;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.parse.Parse;
import com.parse.ParseFacebookUtils;

/**
 * Created by leonardogcsoares on 11/26/2015.
 */
public class EvocoApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        // Required - Initialize the Parse SDK
        Parse.initialize(this);
        FacebookSdk.sdkInitialize(this);
        Parse.setLogLevel(Parse.LOG_LEVEL_DEBUG);

        ParseFacebookUtils.initialize(this);
    }
}

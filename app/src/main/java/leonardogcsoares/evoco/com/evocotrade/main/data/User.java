package leonardogcsoares.evoco.com.evocotrade.main.data;

import java.util.Objects;

/**
 * Created by leonardogcsoares on 11/29/2015.
 */
public class User {

    private final String mUserId;
    private final String mEmail;
    private final String mPhoneNumber;
    private final String mPhotoId;
    private int name;


    public User(String userId, String email, String phoneNumber, String photoId) {
        this.mUserId = userId;
        this.mEmail = email;
        this.mPhoneNumber = phoneNumber;
        this.mPhotoId = photoId;
    }

    public String getUserId() {
        return mUserId;
    }

    public String getEmail() {
        return mEmail;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public String getPhotoId() {
        return mPhotoId;
    }

    public int getName() {
        return name;
    }
}
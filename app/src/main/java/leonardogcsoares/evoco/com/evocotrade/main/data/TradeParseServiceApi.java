package leonardogcsoares.evoco.com.evocotrade.main.data;

import java.util.List;

/**
 * Created by leonardogcsoares on 11/29/2015.
 */
public interface TradeParseServiceApi {



    interface TradeParseServiceCallback<T> {

        void onLoaded(T trade);
    }

    void getAllTradesForUser(String userId, TradeParseServiceCallback<List<Trade>> callback);

    void getBuyTrades(TradeParseServiceCallback<List<Trade>> callback);

    void getSellTrades(TradeParseServiceCallback<List<Trade>> callback);

    void getBuyTrade(String tradeId, TradeParseServiceCallback<Trade> callback);

    void getSellTrade(String tradeId, TradeParseServiceCallback<Trade> callback);


    void saveTrade(Trade trade);
}

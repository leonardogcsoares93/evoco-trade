package leonardogcsoares.evoco.com.evocotrade.main.main.sellers;

import java.util.List;

import leonardogcsoares.evoco.com.evocotrade.main.data.Trade;
import leonardogcsoares.evoco.com.evocotrade.main.data.TradeRepository;
import leonardogcsoares.evoco.com.evocotrade.main.data.UserRepository;
import leonardogcsoares.evoco.com.evocotrade.main.main.buyers.BuyersContract;

/**
 * Created by leonardogcsoares on 11/29/2015.
 */
public class SellersPresenter implements SellersContract.UserActionListener {


    private final TradeRepository mTradeRepository;
    private final UserRepository userRepository;
    private final SellersContract.View mSellerView;

    public SellersPresenter(TradeRepository mTradeRepository, UserRepository userRepository, SellersContract.View mSellerView) {
        this.mTradeRepository = mTradeRepository;
        this.userRepository = userRepository;
        this.mSellerView = mSellerView;
    }

    @Override
    public void openTradeDetails(Trade tradeClicked) {

        mSellerView.showTradeDetailsUi(tradeClicked);
    }

    @Override
    public void loadTrades(boolean forced) {

        if (forced)
            mTradeRepository.refreshData();

        mSellerView.setProgressIndicator(true);
        mTradeRepository.getSellTrades(new TradeRepository.LoadTradesCallback() {
            @Override
            public void onTradesLoaded(List<Trade> trades) {
                mSellerView.setProgressIndicator(false);
                mSellerView.updateToRecyclerView(trades);
            }
        });
    }
}

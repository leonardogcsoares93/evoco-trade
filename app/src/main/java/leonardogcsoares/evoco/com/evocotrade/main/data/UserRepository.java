package leonardogcsoares.evoco.com.evocotrade.main.data;

/**
 * Created by leonardogcsoares on 11/29/2015.
 */
public interface UserRepository {

    interface GetUserCallback {

        void onUserLoaded(User user);
    }


    void getUser(String userId, GetUserCallback callback);

    void saveUser(User user);

}

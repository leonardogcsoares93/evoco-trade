package leonardogcsoares.evoco.com.evocotrade.main.main;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import leonardogcsoares.evoco.com.evocotrade.R;
import leonardogcsoares.evoco.com.evocotrade.main.main.buyers.BuyersFragment;
import leonardogcsoares.evoco.com.evocotrade.main.main.sellers.SellersFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        MainContract.View {

    private static final String TAG = "MainActivity";

    private ViewPager mViewPager;

    private MainContract.UserActionListener mActionsListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.mainActivityToolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.mainActivityDrawerLayout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.mainActivityNavigationView);
        navigationView.setNavigationItemSelectedListener(this);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.mainActivityViewPager);
        setUpViewPager(mViewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.mainActivityTabLayout);
        tabLayout.setupWithViewPager(mViewPager);

        if (!FacebookSdk.isInitialized()) {
            FacebookSdk.sdkInitialize(getApplicationContext());
        }
        mActionsListener = new MainPresenter(this);

    }


    @Override
    protected void onResume() {
        super.onResume();

        mActionsListener.getUserInfoFromFacebookAsync(AccessToken.getCurrentAccessToken(), new MainContract.UserActionListener.GetFacebookInfoCallback() {
            @Override
            public void onUserLoaded(Bitmap bitmap, String name, String email) {
                setNavHeaderCircleImageView(bitmap);
                setNavHeaderNameAndEmail(name, email);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.mainActivityDrawerLayout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /* Options menu configurations */
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    private void setUpViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new BuyersFragment(), getString(R.string.tab_title_buyers));
        adapter.addFragment(new SellersFragment(), getString(R.string.tab_title_sellers));
        viewPager.setAdapter(adapter);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.navMyTrades) {

        } else if (id == R.id.navSettings) {

        } else if (id == R.id.navAboutUs) {

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.mainActivityDrawerLayout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void setNavHeaderCircleImageView(Bitmap bitmap) {
        CircleImageView circleImageView = (CircleImageView) findViewById(R.id.navHeaderProfilePictureView);
        circleImageView.setImageBitmap(bitmap);
    }

    @Override
    public void setNavHeaderNameAndEmail(String name, String email) {
        TextView nameView = (TextView) findViewById(R.id.navHeaderFullName);
        nameView.setText(name);
        TextView emailView = (TextView) findViewById(R.id.navHeaderEmail);
        emailView.setText(email);
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return mFragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
    }


}

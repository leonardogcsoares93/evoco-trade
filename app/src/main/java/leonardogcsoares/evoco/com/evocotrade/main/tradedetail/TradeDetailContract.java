package leonardogcsoares.evoco.com.evocotrade.main.tradedetail;

import leonardogcsoares.evoco.com.evocotrade.main.data.Trade;

/**
 * Created by leonardogcsoares on 11/30/2015.
 */
public interface TradeDetailContract {

    public interface UserActionListener {

        void loadTradeDetailsIntoView(String tradeId, int tradeType);
    }

    public interface View {

        void populateView(Trade trade);

        void setProgressIndicator(boolean loading);
    }
}

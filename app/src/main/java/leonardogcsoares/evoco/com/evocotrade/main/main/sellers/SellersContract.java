package leonardogcsoares.evoco.com.evocotrade.main.main.sellers;

import java.util.List;

import leonardogcsoares.evoco.com.evocotrade.main.data.Trade;

/**
 * Created by leonardogcsoares on 11/29/2015.
 */
public interface SellersContract {

    interface View {

        void setProgressIndicator(boolean b);

        void updateToRecyclerView(List<Trade> trades);

        void showTradeDetailsUi(Trade trade);
    }

    interface UserActionListener {

        void openTradeDetails(Trade tradeClicked);

        void loadTrades(boolean loadTrades);
    }
}

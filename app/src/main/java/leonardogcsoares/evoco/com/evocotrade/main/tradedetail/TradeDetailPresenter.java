package leonardogcsoares.evoco.com.evocotrade.main.tradedetail;

import android.support.annotation.NonNull;

import leonardogcsoares.evoco.com.evocotrade.main.data.Trade;
import leonardogcsoares.evoco.com.evocotrade.main.data.TradeRepository;

/**
 * Created by leonardogcsoares on 11/30/2015.
 */
public class TradeDetailPresenter implements TradeDetailContract.UserActionListener {

    @NonNull
    private final TradeRepository mTradeRepository;
    @NonNull
    private final TradeDetailContract.View mTradeDetailView;

    public TradeDetailPresenter(@NonNull TradeRepository tradeRepository, @NonNull TradeDetailContract.View tradeDetailView) {
        this.mTradeRepository = tradeRepository;
        this.mTradeDetailView = tradeDetailView;
    }


    @Override
    public void loadTradeDetailsIntoView(String tradeId, int tradeType) {
        mTradeDetailView.setProgressIndicator(true);

        if (tradeType == Trade.BUYING) {
            mTradeRepository.getBuyTrade(tradeId, new TradeRepository.GetTradeCallback() {
                @Override
                public void onTradeLoaded(Trade trade) {
                    mTradeDetailView.populateView(trade);
                    mTradeDetailView.setProgressIndicator(false);
                }
            });
        }
        else if (tradeType == Trade.SELLING) {
            mTradeRepository.getSellTrade(tradeId, new TradeRepository.GetTradeCallback() {
                @Override
                public void onTradeLoaded(Trade trade) {
                    mTradeDetailView.populateView(trade);
                    mTradeDetailView.setProgressIndicator(false);
                }
            });
        }

    }
}

package leonardogcsoares.evoco.com.evocotrade.main.login;

import com.parse.ui.ParseLoginDispatchActivity;
import leonardogcsoares.evoco.com.evocotrade.main.main.MainActivity;

/**
 * Created by leonardogcsoares on 11/26/2015.
 */
public class LoginDispatchActivity extends ParseLoginDispatchActivity {
    @Override
    protected Class<?> getTargetClass() {
        return MainActivity.class;
    }
}

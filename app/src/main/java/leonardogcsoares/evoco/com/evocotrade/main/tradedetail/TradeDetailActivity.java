package leonardogcsoares.evoco.com.evocotrade.main.tradedetail;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.LinearLayout;

import leonardogcsoares.evoco.com.evocotrade.R;
import leonardogcsoares.evoco.com.evocotrade.main.data.Trade;

/**
 * Created by leonardogcsoares on 11/29/2015.
 */
public class TradeDetailActivity extends AppCompatActivity implements TradeDetailContract.View{
    public static final String EXTRA_TRADE_ID = "TRADE_ID";
    public static final String EXTRA_TRADE_TYPE = "TRADE_TYPE";

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setContentView(R.layout.activity_trade_detail);

//        TradeDetailPresenter mActionsListener = new TradeDetailPresenter();

    }

    @Override
    public void populateView(Trade trade) {

        AppCompatTextView name = (AppCompatTextView) findViewById(R.id.tradeDetailActivityName);
        AppCompatTextView cityState = (AppCompatTextView) findViewById(R.id.tradeDetailActivityCityState);
        AppCompatTextView country = (AppCompatTextView) findViewById(R.id.tradeDetailActivityCountry);
        AppCompatTextView amount = (AppCompatTextView) findViewById(R.id.tradeDetailActivityAmount);
        AppCompatTextView quote = (AppCompatTextView) findViewById(R.id.tradeDetailActivityQuote);
        AppCompatTextView type = (AppCompatTextView) findViewById(R.id.tradeDetailActivityTypeTrade);
        AppCompatTextView email = (AppCompatTextView) findViewById(R.id.tradeDetailActivityEmail);
        LinearLayout emailLayout = (LinearLayout) findViewById(R.id.tradeDetailActivitySendEmail);

        name.setText(trade.getUser().getName());
        cityState.setText(trade.getCity() + ", " + trade.getState());
        country.setText(trade.getCountry());
        email.setText(trade.getUser().getEmail());

        if (trade.getTradeType() == Trade.BUYING) {
            amount.setText(trade.getBuyingCurrency() + " " + trade.getAmount());
            quote.setText(Float.toString(trade.getQuote()) + " " + trade.getSellingCurrency() + "/" + trade.getBuyingCurrency());
            type.setText(getString(R.string.trade_type_buying));
        }
        if (trade.getTradeType() == Trade.SELLING) {
            amount.setText(trade.getSellingCurrency() + " " + trade.getAmount());
            quote.setText(Float.toString(trade.getQuote()) + " " + trade.getBuyingCurrency() + "/" + trade.getSellingCurrency());
            type.setText(getString(R.string.trade_type_selling));
        }

        // Sends to mTradeDetailPresenter Action to deal with
//        emailLayout.setOnClickListener(mActionListener.onEmailClick());


    }

    @Override
    public void setProgressIndicator(boolean loading) {
        if (loading) {
            View parentView = findViewById(android.R.id.content);
            Snackbar.make(parentView, R.string.trades_loading_progress_indicator, Snackbar.LENGTH_SHORT).show();
        }
    }
}

package leonardogcsoares.evoco.com.evocotrade.main.main.buyers;

import java.util.List;

import leonardogcsoares.evoco.com.evocotrade.main.data.Trade;
import leonardogcsoares.evoco.com.evocotrade.main.data.TradeRepository;
import leonardogcsoares.evoco.com.evocotrade.main.data.UserRepository;

/**
 * Created by leonardogcsoares on 11/29/2015.
 */
public class BuyersPresenter implements BuyersContract.UserActionListener {

    private final TradeRepository mTradeRepository;
    private final UserRepository mUserRepository;
    private final BuyersContract.View mBuyerView;

    public BuyersPresenter(TradeRepository tradeRepository, UserRepository userRepository, BuyersContract.View mBuyerView) {

        this.mTradeRepository = tradeRepository;
        this.mUserRepository = userRepository;
        this.mBuyerView = mBuyerView;
    }

    @Override
    public void openTradeDetails(Trade tradeClicked) {

        mBuyerView.showTradeDetailsUi(tradeClicked);
    }

    @Override
    public void loadTrades(boolean forced) {

        if (forced)
            mTradeRepository.refreshData();

        mBuyerView.setProgressIndicator(true);
        mTradeRepository.getBuyTrades(new TradeRepository.LoadTradesCallback() {
            @Override
            public void onTradesLoaded(List<Trade> trades) {
                mBuyerView.setProgressIndicator(false);
                mBuyerView.updateToRecyclerView(trades);
            }
        });
    }
}

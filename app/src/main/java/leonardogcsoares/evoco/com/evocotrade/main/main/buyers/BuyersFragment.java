package leonardogcsoares.evoco.com.evocotrade.main.main.buyers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import leonardogcsoares.evoco.com.evocotrade.R;
import leonardogcsoares.evoco.com.evocotrade.main.data.Trade;
import leonardogcsoares.evoco.com.evocotrade.main.tradedetail.TradeDetailActivity;

/**
 * Created by leonardogcsoares on 11/29/2015.
 */
public class BuyersFragment extends Fragment implements BuyersContract.View{

    private BuyersContract.UserActionListener mActionsListener;

    private BuyersAdapter mListAdapter;
    private LinearLayoutManager mLinearLayoutManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_buyers, container, false);
        RecyclerView recyclerView = (RecyclerView) root.findViewById(R.id.fragmentBuyersRecyclerView);
        recyclerView.setAdapter(mListAdapter);

//        int numColumns = getContext().getResources().getInteger(R.integer.num_trades_columns);

        recyclerView.setHasFixedSize(true);
        mLinearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLinearLayoutManager);

        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) root.findViewById(R.id.fragmentBuyersSwipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                ContextCompat.getColor(getActivity(), R.color.colorAccent)
        );
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mActionsListener.loadTrades(true);
            }
        });

        return root;
    }

    @Override
    public void setProgressIndicator(boolean loading) {

        if (loading)
            Snackbar.make(getView(), R.string.trades_loading_progress_indicator, Snackbar.LENGTH_SHORT);

    }

    @Override
    public void updateToRecyclerView(List<Trade> trades) {
        mListAdapter.replaceData(trades);
    }

    @Override
    public void showTradeDetailsUi(Trade trade) {
        // Trade detail View
        Intent intent = new Intent(getContext(), TradeDetailActivity.class);
        intent.putExtra(TradeDetailActivity.EXTRA_TRADE_ID, trade.getTradeId());
        intent.putExtra(TradeDetailActivity.EXTRA_TRADE_TYPE, trade.getTradeType());
        startActivity(intent);
    }

    BuyItemListener mItemListener = new BuyItemListener() {
        @Override
        public void onBuyTradeClicked(Trade tradeClicked) {
            mActionsListener.openTradeDetails(tradeClicked);
        }
    };


    public interface BuyItemListener {

        void onBuyTradeClicked(Trade tradeClicked);
    }


    private static class BuyersAdapter extends RecyclerView.Adapter<BuyersAdapter.ViewHolder>{

        private List<Trade> mTrades;
        private BuyItemListener mItemListener;

        public BuyersAdapter(List<Trade> trades, BuyItemListener listener) {
            setList(trades);
            mItemListener = listener;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            Context context = parent.getContext();
            LayoutInflater inflater = LayoutInflater.from(context);
            View tradeView = inflater.inflate(R.layout.trades_list_card_view, parent, false);
            return new ViewHolder(tradeView, mItemListener);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            Trade buyTrade = mTrades.get(position);

            holder.image.setImageBitmap(null);
            holder.name.setText(buyTrade.getUser().getName());
            holder.cityState.setText(buyTrade.getUser().getName());
            holder.country.setText(buyTrade.getUser().getName());
            holder.amount.setText(buyTrade.getUser().getName());
            holder.buyerCurrency.setText(buyTrade.getUser().getName());
            holder.currencyConversion.setText(buyTrade.getUser().getName());
            holder.quote.setText(buyTrade.getUser().getName());

        }

        private void replaceData(List<Trade> trades) {
            setList(trades);
            notifyDataSetChanged();
        }

        private void setList (List<Trade> trades) {
            mTrades = trades;
        }

        @Override
        public int getItemCount() {
            return mTrades.size();
        }

        private Trade getItem(int position) {
            return mTrades.get(position);
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            BuyItemListener mItemListener;

            CircleImageView image;
            AppCompatTextView name;
            AppCompatTextView cityState;
            AppCompatTextView country;
            AppCompatTextView amount;
            AppCompatTextView buyerCurrency;
            AppCompatTextView currencyConversion;
            AppCompatTextView quote;

            public ViewHolder(View itemView, BuyItemListener listener) {
                super(itemView);

                mItemListener = listener;
                itemView.setOnClickListener(this);

                image = (CircleImageView) itemView.findViewById(R.id.tradesListCardViewUserImage);
                name = (AppCompatTextView) itemView.findViewById(R.id.tradesListCardViewUserName);
                cityState = (AppCompatTextView) itemView.findViewById(R.id.tradesListCardViewUserLocation);
                country = (AppCompatTextView) itemView.findViewById(R.id.buyersCardViewUserCountry);
                amount = (AppCompatTextView) itemView.findViewById(R.id.tradesListCardViewTotalAmount);
                buyerCurrency = (AppCompatTextView) itemView.findViewById(R.id.tradesListCardViewCurrency);
                currencyConversion = (AppCompatTextView) itemView.findViewById(R.id.tradesListCardViewCurrencyConversion);
                quote = (AppCompatTextView) itemView.findViewById(R.id.tradesListCardViewQuote);

            }
            @Override
            public void onClick(View v) {
                int position = getAdapterPosition();
                Trade trade = getItem(position);
                mItemListener.onBuyTradeClicked(trade);

            }

        }
    }

}

package leonardogcsoares.evoco.com.evocotrade.main.data;

import java.util.List;

/**
 * Created by leonardogcsoares on 11/29/2015.
 */
public class InMemoryTradesRepository implements TradeRepository, UserRepository {

    private final UserParseServiceApi mUserParseServiceApi;
    private final TradeParseServiceApi mTradeParseServiceApi;


    List<Trade> mCachedBuyTrades;
    List<Trade>  mCachedSellTrades;

    public InMemoryTradesRepository(UserParseServiceApi userParseServiceApi, TradeParseServiceApi tradeParseServiceApi) {
        this.mUserParseServiceApi = userParseServiceApi;
        this.mTradeParseServiceApi = tradeParseServiceApi;
    }

    // Trades Repository

    @Override
    public void getBuyTrades(final LoadTradesCallback callback) {

        if (mCachedBuyTrades == null) {
            mTradeParseServiceApi.getBuyTrades(new TradeParseServiceApi.TradeParseServiceCallback<List<Trade>>() {
                @Override
                public void onLoaded(List<Trade> trade) {
                    mCachedBuyTrades = trade;
                    callback.onTradesLoaded(mCachedBuyTrades);
                }
            });
        }
        else {
            callback.onTradesLoaded(mCachedBuyTrades);
        }
    }

    @Override
    public void getSellTrades(final LoadTradesCallback callback) {

        if (mCachedSellTrades == null) {
            mTradeParseServiceApi.getSellTrades(new TradeParseServiceApi.TradeParseServiceCallback<List<Trade>>() {
                @Override
                public void onLoaded(List<Trade> trade) {
                    mCachedSellTrades = trade;
                    callback.onTradesLoaded(mCachedSellTrades);
                }
            });
        }
        else {
            callback.onTradesLoaded(mCachedSellTrades);
        }
    }

    @Override
    public void getTradesForUser(String userId, final LoadTradesCallback callback) {
        mTradeParseServiceApi.getAllTradesForUser(userId, new TradeParseServiceApi.TradeParseServiceCallback<List<Trade>>() {
            @Override
            public void onLoaded(List<Trade> trade) {
                callback.onTradesLoaded(trade);
            }
        });
    }

    @Override
    public void getBuyTrade(String tradeId, final GetTradeCallback callback) {

        if (mCachedBuyTrades != null) {
            for (Trade t : mCachedBuyTrades)
                if (t.getTradeId().equals(tradeId)) {
                    callback.onTradeLoaded(t);
                    return;
                }
        }

        mTradeParseServiceApi.getBuyTrade(tradeId, new TradeParseServiceApi.TradeParseServiceCallback<Trade>() {
                @Override
                public void onLoaded(Trade trade) {
                    callback.onTradeLoaded(trade);
                }
            });

    }

    @Override
    public void getSellTrade(String tradeId, final GetTradeCallback callback) {

        if (mCachedSellTrades != null) {
            for (Trade t : mCachedSellTrades)
                if (t.getTradeId().equals(tradeId)) {
                    callback.onTradeLoaded(t);
                    return;
                }
        }


        mTradeParseServiceApi.getSellTrade(tradeId, new TradeParseServiceApi.TradeParseServiceCallback<Trade>() {
                @Override
                public void onLoaded(Trade trade) {
                    callback.onTradeLoaded(trade);
                }
            });

    }

    @Override
    public void saveTrade(Trade trade) {
        mTradeParseServiceApi.saveTrade(trade);
        refreshData();
    }

    @Override
    public void refreshData() {
        mCachedBuyTrades = null;
        mCachedSellTrades = null;
    }


    ///// User Repository

    @Override
    public void getUser(String userId, final GetUserCallback callback) {
        mUserParseServiceApi.getUser(userId, new UserParseServiceApi.UserParseServiceCallback<User>() {
            @Override
            public void onLoaded(User user) {
                callback.onUserLoaded(user);
            }
        });
    }

    @Override
    public void saveUser(User user) {
        mUserParseServiceApi.updateUser(user);
    }
}

package leonardogcsoares.evoco.com.evocotrade.main.data;

/**
 * Created by leonardogcsoares on 11/29/2015.
 */
public interface UserParseServiceApi {

    interface UserParseServiceCallback<T> {

        void onLoaded(T user);
    }

    void getUser(String userId, UserParseServiceCallback<User> callback);

    void updateUser(User user);
}

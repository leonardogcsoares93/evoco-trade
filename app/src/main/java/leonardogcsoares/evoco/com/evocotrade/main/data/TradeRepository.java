package leonardogcsoares.evoco.com.evocotrade.main.data;

import java.util.List;

/**
 * Created by leonardogcsoares on 11/29/2015.
 */
public interface TradeRepository {

    interface LoadTradesCallback {

        void onTradesLoaded(List<Trade> trades);
    }

    interface GetTradeCallback {

        void onTradeLoaded(Trade trade);
    }


    void getBuyTrades(LoadTradesCallback callback);

    void getSellTrades(LoadTradesCallback callback);

    void getTradesForUser(String userId, LoadTradesCallback callback);

    void getBuyTrade(String tradeId, GetTradeCallback callback);

    void getSellTrade(String tradeId, GetTradeCallback callback);

    void saveTrade(Trade trade);

    void refreshData();
}

package leonardogcsoares.evoco.com.evocotrade.main.main;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;

/**
 * Created by leonardogcsoares on 11/29/2015.
 */
public class MainPresenter implements MainContract.UserActionListener {

    private final MainContract.View mMainView;

    public MainPresenter(@NonNull MainContract.View mMainView) {
        this.mMainView = mMainView;
    }

    @Override
    public void getUserInfoFromFacebookAsync(AccessToken accessToken, final GetFacebookInfoCallback callback) {
        GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        // Sets the Profile picture accordingly, downloads from Facebook and sets it.
                        try {
                            setProfilePictureAsync(object.getJSONObject("picture")
                                    .getJSONObject("data")
                                    .getString("url"), object.getString("name"), object.getString("email"), callback);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
        // Retrieve id, name and email from Facebook
        Bundle fields = new Bundle();
        fields.putString("fields", "id,name,email,picture");
        graphRequest.setParameters(fields);
        graphRequest.executeAsync();
    }

    public void setProfilePictureAsync(final String url, final String name, final String email, final GetFacebookInfoCallback callback) {
        new AsyncTask<Void, Void, Bitmap>() {

            @Override
            protected Bitmap doInBackground(Void... params) {
                try {

                    // Log.d(TAG, url); // Prints url link before retrieval
                    URL imageURL = new URL(url);
                    return BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());

                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }
            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                // After super.onPostExecute, all work resumes on the main thread.
                callback.onUserLoaded(bitmap, name, email);
            }
        }.execute();
    }
}

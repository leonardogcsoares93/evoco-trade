package leonardogcsoares.evoco.com.evocotrade.main.data;

/**
 * Created by leonardogcsoares on 11/29/2015.
 */
public class Trade {

    public static final int BUYING = 0;
    public static final int SELLING = 1;


    private final int mTradeType;
    private String mTradeId;
    private final User mUser;
    private int mAmount;
    private float mQuote;
    private String mBuyingCurrency;
    private String mSellingCurrency;
    private String mCity;
    private String mState;
    private String mCountry;


    public Trade(User mUser, int mTradeType, int mAmount, float mQuote, String mBuyingCurrency, String mSellingCurrency, String mCity, String mState, String mCountry) {
        this.mUser = mUser;
        this.mTradeType = mTradeType;
        this.mAmount = mAmount;
        this.mQuote = mQuote;
        this.mBuyingCurrency = mBuyingCurrency;
        this.mSellingCurrency = mSellingCurrency;
        this.mCity = mCity;
        this.mState = mState;
        this.mCountry = mCountry;
    }

    public User getUser() {
        return mUser;
    }

    public void setTradeId(String tradeId) {
        mTradeId = tradeId;
    }

    public String getTradeId() {
        return mTradeId;
    }

    public int getTradeType() {
        return mTradeType;
    }

    public int getAmount() {
        return mAmount;
    }

    public float getQuote() {
        return mQuote;
    }

    public String getBuyingCurrency() {
        return mBuyingCurrency;
    }

    public String getSellingCurrency() {
        return mSellingCurrency;
    }

    public String getCity() {
        return mCity;
    }

    public String getState() {
        return mState;
    }

    public String getCountry() {
        return mCountry;
    }

}

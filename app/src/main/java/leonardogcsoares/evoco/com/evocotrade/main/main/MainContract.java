package leonardogcsoares.evoco.com.evocotrade.main.main;

import android.content.Context;
import android.graphics.Bitmap;

import com.facebook.AccessToken;
import com.parse.ParseUser;

import leonardogcsoares.evoco.com.evocotrade.main.data.User;

/**
 * Created by leonardogcsoares on 11/29/2015.
 */
public interface MainContract {

    interface View {

        void setNavHeaderCircleImageView(Bitmap bitmap);

        void setNavHeaderNameAndEmail(String name, String email);
    }

    interface UserActionListener {

        interface GetFacebookInfoCallback {
            void onUserLoaded(Bitmap bitmap, String name, String email);
        }

        void getUserInfoFromFacebookAsync(AccessToken accessToken, GetFacebookInfoCallback callback);
    }
}

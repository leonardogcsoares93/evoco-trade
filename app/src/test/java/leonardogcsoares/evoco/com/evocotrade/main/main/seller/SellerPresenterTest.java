package leonardogcsoares.evoco.com.evocotrade.main.main.seller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;


import leonardogcsoares.evoco.com.evocotrade.main.data.Trade;
import leonardogcsoares.evoco.com.evocotrade.main.data.TradeRepository;
import leonardogcsoares.evoco.com.evocotrade.main.data.User;
import leonardogcsoares.evoco.com.evocotrade.main.data.UserRepository;
import leonardogcsoares.evoco.com.evocotrade.main.main.buyers.BuyersContract;
import leonardogcsoares.evoco.com.evocotrade.main.main.buyers.BuyersPresenter;
import leonardogcsoares.evoco.com.evocotrade.main.main.sellers.SellersContract;
import leonardogcsoares.evoco.com.evocotrade.main.main.sellers.SellersPresenter;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

/**
 * Created by leonardogcsoares on 11/29/2015.
 */
public class SellerPresenterTest {

    private User user = new User("userId", "email", "phoneNUmber", "photoId");
    private List<Trade> TRADES = new ArrayList<Trade>(1);

    @Mock
    private TradeRepository mTradeRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private SellersContract.View mSellerView;

    private SellersPresenter mSellerPresenter;

    @Captor
    private ArgumentCaptor<TradeRepository.LoadTradesCallback> mLoadTradesCallbackCaptor;


    @Before
    public void setUpMainPresenter() {

        MockitoAnnotations.initMocks(this);
        mSellerPresenter = new SellersPresenter(mTradeRepository, userRepository, mSellerView);

        TRADES.add(new Trade(user, Trade.BUYING, 100, 2.32f, "USD", "BRL", "City", "State", "Country"));
    }



    @Test
    public void loadNotesFromRepositoryAndLoadIntoRecyclerView() {


        mSellerPresenter.loadTrades(true);
        verify(mSellerView).setProgressIndicator(true);

        verify(mTradeRepository).getSellTrades(mLoadTradesCallbackCaptor.capture());
        mLoadTradesCallbackCaptor.getValue().onTradesLoaded(TRADES);

        verify(mSellerView).setProgressIndicator(false);
        verify(mSellerView).updateToRecyclerView(TRADES);
    }

    @Test
    public void clickOnTrade_showTradeDetailView() {

        mSellerPresenter.openTradeDetails(TRADES.get(0));
        verify(mSellerView).showTradeDetailsUi(TRADES.get(0));
    }
}

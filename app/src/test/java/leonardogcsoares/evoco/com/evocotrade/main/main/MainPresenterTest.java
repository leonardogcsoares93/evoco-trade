package leonardogcsoares.evoco.com.evocotrade.main.main;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.test.mock.MockContext;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import leonardogcsoares.evoco.com.evocotrade.R;
import leonardogcsoares.evoco.com.evocotrade.main.data.TradeRepository;
import leonardogcsoares.evoco.com.evocotrade.main.data.UserRepository;
import leonardogcsoares.evoco.com.evocotrade.main.main.MainActivity;
import leonardogcsoares.evoco.com.evocotrade.main.main.MainContract;
import leonardogcsoares.evoco.com.evocotrade.main.main.MainPresenter;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

/**
 * Created by leonardogcsoares on 11/29/2015.
 */
public class MainPresenterTest {

    private static final String TAG = "MainPresenterTest";

    @Mock
    private TradeRepository tradeRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private MainContract.View mMainView;

    @Mock
    Bundle bundle;

    private MainPresenter mMainPresenter;

    @Before
    public void setUpMainPresenter() {

        MockitoAnnotations.initMocks(this);
        mMainPresenter = new MainPresenter(mMainView);

    }

    @Captor
    private ArgumentCaptor<MainContract.UserActionListener.GetFacebookInfoCallback> mFacebookInfoCallbackCaptor;

    @Test
    public void loadFacebookInfoIntoNavigationHeader() {

    }

}

package leonardogcsoares.evoco.com.evocotrade.main.tradedetail;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import leonardogcsoares.evoco.com.evocotrade.main.data.Trade;
import leonardogcsoares.evoco.com.evocotrade.main.data.TradeRepository;
import leonardogcsoares.evoco.com.evocotrade.main.data.User;
import leonardogcsoares.evoco.com.evocotrade.main.tradedetail.TradeDetailContract;
import leonardogcsoares.evoco.com.evocotrade.main.tradedetail.TradeDetailPresenter;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by leonardogcsoares on 11/30/2015.
 */
public class TradeDetailPresenterTest {

    @Mock
    TradeDetailContract.View mTradeDetailView;

    @Mock
    TradeRepository mTradeRepository;

    private List<Trade> TRADES = new ArrayList<Trade>(1);
    private User user = new User("Leonardo Soares", "leo@ufmg", "97196566", "somePhotoIdString");

    TradeDetailPresenter mTradeDetailPresenter;

    @Captor
    private ArgumentCaptor<TradeRepository.GetTradeCallback> mGetTradeCaptor;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mTradeDetailPresenter = new TradeDetailPresenter(mTradeRepository, mTradeDetailView);

        TRADES.add(new Trade(user, Trade.SELLING, 100, 2.32f, "USD", "BRL", "Belo Horizonte", "Minas Gerais", "Brazil"));
        TRADES.add(new Trade(user, Trade.BUYING, 100, 2.32f, "USD", "BRL", "Belo Horizonte", "Minas Gerais", "Brazil"));
    }

    @Test
    public void loadBuyTradeClickFromRepositoryIntoView() {

        mTradeDetailPresenter.loadTradeDetailsIntoView(TRADES.get(0).getTradeId(), Trade.BUYING);
        mTradeDetailView.setProgressIndicator(true);
        verify(mTradeRepository).getBuyTrade(eq(TRADES.get(0).getTradeId()), mGetTradeCaptor.capture());
        mGetTradeCaptor.getValue().onTradeLoaded(TRADES.get(0));

        verify(mTradeDetailView).populateView(TRADES.get(0));
        mTradeDetailView.setProgressIndicator(false);
    }

    @Test
    public void loadSellTradeClickFromRepositoryIntoView() {

        mTradeDetailPresenter.loadTradeDetailsIntoView(TRADES.get(0).getTradeId(), Trade.SELLING);
        mTradeDetailView.setProgressIndicator(true);
        verify(mTradeRepository).getSellTrade(eq(TRADES.get(0).getTradeId()), mGetTradeCaptor.capture());
        mGetTradeCaptor.getValue().onTradeLoaded(TRADES.get(0));

        verify(mTradeDetailView).populateView(TRADES.get(0));
        mTradeDetailView.setProgressIndicator(false);
    }


}

package leonardogcsoares.evoco.com.evocotrade.main.main.buyer;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;


import leonardogcsoares.evoco.com.evocotrade.main.data.Trade;
import leonardogcsoares.evoco.com.evocotrade.main.data.TradeRepository;
import leonardogcsoares.evoco.com.evocotrade.main.data.User;
import leonardogcsoares.evoco.com.evocotrade.main.data.UserRepository;
import leonardogcsoares.evoco.com.evocotrade.main.main.buyers.BuyersContract;
import leonardogcsoares.evoco.com.evocotrade.main.main.buyers.BuyersPresenter;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

/**
 * Created by leonardogcsoares on 11/29/2015.
 */
public class BuyerPresenterTest {

    private User user = new User("userId", "email", "phoneNUmber", "photoId");
    private List<Trade> TRADES = new ArrayList<Trade>(1);

    @Mock
    private TradeRepository mTradeRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private BuyersContract.View mBuyerView;

    private BuyersPresenter mBuyersPresenter;

    @Captor
    private ArgumentCaptor<TradeRepository.LoadTradesCallback> mLoadTradesCallbackCaptor;


    @Before
    public void setUpMainPresenter() {

        MockitoAnnotations.initMocks(this);
        mBuyersPresenter = new BuyersPresenter(mTradeRepository, userRepository, mBuyerView);

        TRADES.add(new Trade(user, Trade.BUYING, 100, 2.32f, "USD", "BRL", "City", "State", "Country"));
    }



    @Test
    public void loadNotesFromRepositoryAndLoadIntoRecyclerView() {


        mBuyersPresenter.loadTrades(true);
        verify(mBuyerView).setProgressIndicator(true);

        verify(mTradeRepository).getBuyTrades(mLoadTradesCallbackCaptor.capture());
        mLoadTradesCallbackCaptor.getValue().onTradesLoaded(TRADES);

        verify(mBuyerView).setProgressIndicator(false);
        verify(mBuyerView).updateToRecyclerView(TRADES);
    }

    @Test
    public void clickOnTrade_showTradeDetailView() {

        mBuyersPresenter.openTradeDetails(TRADES.get(0));
        verify(mBuyerView).showTradeDetailsUi(TRADES.get(0));
    }
}

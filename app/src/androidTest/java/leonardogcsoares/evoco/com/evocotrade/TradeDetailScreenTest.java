package leonardogcsoares.evoco.com.evocotrade;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import leonardogcsoares.evoco.com.evocotrade.main.tradedetail.TradeDetailActivity;

/**
 * Created by leonardogcsoares on 11/30/2015.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class TradeDetailScreenTest {


    @Rule
    public ActivityTestRule<TradeDetailActivity> mTradeDetailActivityTestRule =
            new ActivityTestRule<>(TradeDetailActivity.class);

}
